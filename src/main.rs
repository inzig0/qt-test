use qt_core::{ QString, QPtr };
use qt_widgets::*;

struct App {
    layout: QWidget,
    
}

fn main() {
    QApplication::init(|_| unsafe {
        let big_cheese = QWidget::new_0a();
        let little_cheese = QVBoxLayout::new_1a(&big_cheese);

        let label = QLabel::new();
        label.set_text(&QString::from_std_str("hellorld"));

        let button_cheese = QHBoxLayout::new_0a();

        let boom_button = QPushButton::from_q_string(&QString::from_std_str("le kaboom button"));
        // the program could live without this
        boom_button.set_default(true);

        let cheese_button = QPushButton::from_q_string(&QString::from_std_str("cheese!"));

        // I'd like to make this work some day...
        //boom_button.clicked().connect(QPtr::from_raw(&panic()));

        little_cheese.add_widget_1a(&label);
        button_cheese.add_widget_1a(&boom_button);
        button_cheese.add_widget_1a(&cheese_button);

        little_cheese.add_layout_1a(&button_cheese);

        big_cheese.show();
        QApplication::exec()
    });
}
